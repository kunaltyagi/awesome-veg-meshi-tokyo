# awesome-veg-meshi-tokyo



## List of recommended Vegaetarian Places in Tokyo

* [T's tantan](https://www.happycow.net/reviews/ts-tantan-tokyo-29533) (Inside Tokyo Station) (Vegan)
* [Soranoiro](https://www.happycow.net/reviews/soranoiro-tokyo-120511)
* [Great Lakes](https://www.happycow.net/reviews/great-lakes-tokyo-190401) (Vegan)
* [ISKON Sunday Feast](https://www.iskconjapan.com/) (accompanies a spiritual element as well)
* [Veg Kitchen](https://tabelog.com/en/tokyo/A1311/A131101/13168026/)
* [Veg Herb Saga](http://www.vegeherbsaga.com/menu_en.html)
* [Shreeji Foods](https://www.shreejifoods.jp/contact)
* [Falafel Brothers](https://www.falafelbrothers.jp/)

## List of Activities

* [Tokyo Vegan Meetup](https://tokyovegan.org/)
